<head><link rel="stylesheet" href="index.css"></head><div id="content">
<?php

//Simply put, this simplifies everything
require_once('../sql_connect.php');
require_once('../coinhive.php'); //Important - This site uses Coinhive
$n = $_POST["name"];
$n = strtolower($n);
$p = $_POST["password"];

//If the name field is empty, don't continue (used so if someone links here, the vistors won't get worried about being 'hacked')
if (empty($n)){
	echo "<meta http-equiv=\"refresh\" content=\"0; url=home.php\" />";exit;
}
//Automated requests hate this (verifies captcha is completed)
if ($mined && $mined->success) {}else{
	echo 'Please complete captcha. This page will automatically go back.';
	echo "<meta http-equiv=\"refresh\" content=\"4; url=home.php\" />";exit;}

//Ensures name is in database
$sql = "Select * from hotel where name=?;";
$stmt = mysqli_stmt_init($dbc);
if(!mysqli_stmt_prepare($stmt, $sql)) {
	echo "Sql statement failed";exit;
}else{
	mysqli_stmt_bind_param($stmt, "s", $n);
	mysqli_stmt_execute($stmt);
	
	$result = mysqli_stmt_get_result($stmt);
	$row = mysqli_fetch_assoc($result);
}


//Creates a random seed, which is used for the cookie
$seed = str_split('abcdefghijklmnopqrstuvwxyz'
                 .'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                 .'0123456789');
shuffle($seed);
$cookie = ''; foreach (array_rand($seed, 30) as $k) $cookie .= $seed[$k];

//Looks at a calendar
$date = date("Y-m-d");


$corp = $row['password'];//Means, CORrect Password
$corn = $row['name'];//Similarly, CORrect Name
$wname = ucfirst(strtolower($corn));//Makes first letter of name uppercase. This variable is only used in 'Hello, Yourname'

//Makes sure the password and name match what the database has
if (password_verify($p, $corp) and $corn == $n){
	echo "Hello ";
	echo $wname . '<br> <a href="logout.php">Logout</a><br><a href="manage-sharing.php">Sharing</a><br>';
	
	//Creates a real yummy cookie
	$sql = "insert into cookies values(?,?,null,?);";
	$stmt = mysqli_stmt_init($dbc);
	if(!mysqli_stmt_prepare($stmt, $sql)) {
	echo "Sql statement failed";exit;}
	mysqli_stmt_bind_param($stmt, "sss", $n, $cookie, $date);
	mysqli_stmt_execute($stmt);
	
	//And shares it with the user for 1/12 of a year
	setcookie('User', $cookie, time() + (86400 * 30), "/", '', true);
	
	//Show all the images in the user's folder
	$all_files = glob($row[folder]);
	  for ($i=0; $i<count($all_files); $i++)
    {
      $image_name = $all_files[$i];
      $supported_format = array('gif','jpg','jpeg','png');
      $ext = strtolower(pathinfo($image_name, PATHINFO_EXTENSION));
      if (in_array($ext, $supported_format))
          {
            echo '<img src="'.$image_name .'" alt="'.$image_name.'" width="'. 400 .'" />'."";
          } else {
              continue;
          }
    }
	//Then the sad part. If the name/password didn't work, it comes here.
}else{
	echo "Incorrect name or password. This page will automatically go back.";
	echo "<meta http-equiv=\"refresh\" content=\"4; url=home.php\" />";exit;
}


//Bye bye!
mysqli_close($dbc);

?>