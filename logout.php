<head><link rel="stylesheet" href="index.css"></head><div id="content">
<?php

//Simply put, this simplifies everything
require_once('../sql_connect.php');
$cookie = $_COOKIE['User'];

//Makes sure that cookies are real, and not those raisin ones
	$sql = "select name from cookies where cookie=?;";
	$stmt = mysqli_stmt_init($dbc);
	if(!mysqli_stmt_prepare($stmt, $sql)){echo "Error at 1";exit;}
	mysqli_stmt_bind_param($stmt, "s", $cookie);
	mysqli_stmt_execute($stmt);
	$result = mysqli_stmt_get_result($stmt);
	$row = mysqli_fetch_array($result);
	$name = $row[0];

//If there's no cookies available, or only raisin types, issue an error
if ($name == ""){
	echo "An error occurred";
	setcookie("User", "", time() - 3600, "/");
	echo '<meta http-equiv="refresh" content="2; url=home.php" />';
	exit;
}

//The browser eats the cookie (deletes cookie on user's side)
setcookie("User", "", time() - 3600, "/");	

//Has server verify that the cookie can't be used again
	$sql = "delete from cookies where cookie=?;";
	$stmt = mysqli_stmt_init($dbc);
	if(!mysqli_stmt_prepare($stmt, $sql)){echo "Error at 2";exit;}
	mysqli_stmt_bind_param($stmt, "s", $cookie);
	if(!mysqli_stmt_execute($stmt)){echo "Error at 2.1";}

//Some random text the user can read
echo "You have been logged out";
echo '<meta http-equiv="refresh" content="3; url=home.php" />';


//Bye bye!
mysqli_close($dbc);

?>