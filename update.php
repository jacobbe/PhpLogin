<head><link rel="stylesheet" href="index.css"><style>#center{width:900px;margin-left: -450px;}</style></head><div id="center">
<?php

//Simply put, this simplifies everything
require_once('../sql_connect.php');
require_once('../coinhive.php');
$n = $_POST["name"];
$n = strtolower($n);
$p = $_POST["password"];
$u = $_POST["update"];

//They have to verify it's not a brute force attack
if ($mined && $mined->success) {
	}else{
	echo 'Please complete captcha. This page will automatically go back.';
	echo "<meta http-equiv=\"refresh\" content=\"4; url=update.html\" />";exit;}

//If the name field is empty, don't continue (used so if someone links here, the vistors won't get worried about being 'hacked')
if (empty($n)){
	echo "Please enter a name and password. This page will automatically go back.";
	echo "<meta http-equiv=\"refresh\" content=\"4; url=update.html\" />";
	exit;
}

//Encrypts the new password
$enc = password_hash($u, PASSWORD_BCRYPT);

//Retrives name from database
$sql = 'Select * from hotel where name=?;';
$stmt = mysqli_stmt_init($dbc);
if(!mysqli_stmt_prepare($stmt, $sql)) {
	echo "Sql statement failed";exit;
}else{
	mysqli_stmt_bind_param($stmt, "s", $n);
	mysqli_stmt_execute($stmt);
	$result = mysqli_stmt_get_result($stmt);
	$row = mysqli_fetch_assoc($result);
}

//Puts name and password as their own variables
$corp = $row[password];//Means CORrect Password
$corn = $row[name];//Similarly, CORrect Name

//Checks if the name/password is real
if ((password_verify($p, $corp) and $corn == $n)){
	//updates password in database
	$sql = 'update hotel set password=? where name=?;';
	$stmt = mysqli_stmt_init($dbc);
	if(!mysqli_stmt_prepare($stmt, $sql)) {
	echo "Sql statement failed";exit;
	}else{
	mysqli_stmt_bind_param($stmt, "ss", $enc, $n);
	mysqli_stmt_execute($stmt);}

	//Deletes cookie from browser
setcookie("User", "", time() - 3600);
	
	//Spills all the cookies and throws them away
	$sql = 'delete from cookies WHERE name=?;';
	$stmt = mysqli_stmt_init($dbc);
	if(!mysqli_stmt_prepare($stmt, $sql)) {
	echo "Sql statement failed";exit;
	}else{
	mysqli_stmt_bind_param($stmt, "s", $n);
	mysqli_stmt_execute($stmt);}
	
	//Confirmation
	echo "Password has been updated<br>";
echo "<meta http-equiv=\"refresh\" content=\"4; url=home.php\" />";

//Sadly, if the name/password don't work, it shows this
}else{
	echo "Name or password is incorrect. This page will automatically go back.";
	echo "<meta http-equiv=\"refresh\" content=\"4; url=update.html\" />";
}

//Bye bye!
mysqli_close($dbc);

?>