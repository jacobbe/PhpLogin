<head><link rel="stylesheet" href="index.css"><style>a {color: black;} .inline {display: inline;}
.link-button {background: none;border: none;color: black;font-size: 1em; text-decoration: none; font-family: Arial, Helvetica, sans-serif;}</style></head>
<?php
//Simply put, this simplifies everything
require_once('../sql_connect.php');
$cookie = $_COOKIE['User'];
$s = $_POST['s'];

//When there's no cookies available, you should search elsewhere (redirects to homepage)
if (empty($cookie)){echo '<meta http-equiv="refresh" content="0; url=home.php" >';exit;}

//If user clicked on link to delete link (or submitted post['s'] another way), then it should attempt to delete the link
if(!empty($s)){
	
//Finds who the cookie really belongs to, and makes sure it wasn't stolen by another person
	$sql = "select name from cookies where cookie=?;";
	$stmt = mysqli_stmt_init($dbc);
	mysqli_stmt_prepare($stmt, $sql);
	mysqli_stmt_bind_param($stmt, "s", $cookie);
	mysqli_stmt_execute($stmt);
	$result = mysqli_stmt_get_result($stmt);
	$row = mysqli_fetch_assoc($result);$namever = $row[0];
	
//Tries to find a link in the table, and sees who created it
	
	$sql = "select name from shared where id =?;";
	$stmt = mysqli_stmt_init($dbc);
	mysqli_stmt_prepare($stmt, $sql);
	mysqli_stmt_bind_param($stmt, "s", $s);
	mysqli_stmt_execute($stmt);
	$result = mysqli_stmt_get_result($stmt);
	$row = mysqli_fetch_assoc($result);$nameveri = $row[0];
	
	//If the link isn't valid, don't do anything
	if($namever != $nameveri){
	echo "<meta http-equiv=\"refresh\" content=\"0; url=manage-sharing.php\" />";exit;}
	else{
//but if it is, delete it
	
	$sql = "delete from shared where id=?;";
	$stmt = mysqli_stmt_init($dbc);
	mysqli_stmt_prepare($stmt, $sql);
	mysqli_stmt_bind_param($stmt, "s", $s);
	mysqli_stmt_execute($stmt);
}}

//Again, makes sure the cookie wasn't stolen by their evil cat
	$sql = "select name from cookies where cookie=?;";
	$stmt = mysqli_stmt_init($dbc);
	if(!mysqli_stmt_prepare($stmt, $sql)){echo "Error at 1";exit;}
	mysqli_stmt_bind_param($stmt, "s", $cookie);
	mysqli_stmt_execute($stmt);
	$result = mysqli_stmt_get_result($stmt);
	$row = mysqli_fetch_array($result);
	$name = $row[0];

//Looks up how much tax the server owner has to pay to the EU (retrieves all links that the user has created)
	$sql = "select id from shared where name=?;";
	$stmt = mysqli_stmt_init($dbc);
	if(!mysqli_stmt_prepare($stmt, $sql)){echo "Error at 2";exit;}
	mysqli_stmt_bind_param($stmt, "s", $name);
	mysqli_stmt_execute($stmt);
	$response = mysqli_stmt_get_result($stmt);

//If the user decides to make more taxes for the server owner, then this happens. If not, then, well, there aren't any links displayed
if($response){
echo '<a href="share.php" style="color:blue;">Create link</a><br>';
echo '<table align="left"
cellspacing="5" cellpadding="8">
<tr><td width=160 align="left"><b>Link</b></td>
<td align="left"><b>Delete</b></td></tr>';

//Fine, the last step just set up the table. This one actually puts out all the links
while($row = mysqli_fetch_array($response)){
$id = $row['id'];
echo '<tr ><td align="left"><a href="/login/?s=' . $id . '">' . $id . '</a></td><td align="left">';

echo '
<form method="post" action="manage-sharing.php" class="inline">
  <input type="hidden" name="s" value="'.$id.'">
  <button type="submit" name="submit_param" value="submit_value" class="link-button">
    X
  </button>
</form>';

echo '</tr>';
}
echo '</table>';

} else {

echo "Couldn't issue database query<br>";

echo mysqli_error($dbc);

}
//Bye bye!
mysqli_close($dbc);

?>